// syncmap provides a simple generic goroutine safe map type
//
//  // If you plan on doing multiple operations on the map
//  // which have to be synchronized use the WithLock method!
//  m := syncmap.New[string, int]()
//  m.Set("a", 1)
//  m.Set("b", 2)
//  m.Set("c", 3)
//  if v, ok := m.Get("a"); ok {
//      fmt.Println("found a! it's value is:", v)
//  }
//
//  m.Delete("a") // delete a single key
//  m.Delete("b", "c") // delete a bunch of keys at once
//  m.Reset() // clear the map
//
//  // Run multiple operations in sync
//  m.WithLock(func(lm LockedMap[string, int]) {
//      if value, ok := lm.Get("a"); ok {
//          lm.Set("b", value)
//      }
//  })
//
package syncmap

import "sync"

// LockedMap is a map which is already locked.
// It's Methods do not lock and are therefore not goroutine safe.
// You only use this within the WithLock method of Map.
type LockedMap[K comparable, V any] interface {
	Set(key K, value V)
	Get(key K) (value V, found bool)
	Delete(keys ...K)
	Reset()
	Len() int
	Keys() []K
}

// Map is a generic, goroutine safe, maplike container.
// It uses a mutex to be goroutine safe.
type Map[K comparable, V any] interface {
	Set(key K, value V)
	Get(key K) (value V, found bool)
	Delete(keys ...K)
	// Reset removes all elements from the map
	Reset()
	Len() int

	Keys() []K

	// WithLock provides a way to run multiple operations on a map
	// by holding the lock for the entire time of the callback
	//
	//  m := syncmap.New[string, int]()
	//
	//  // BAD BAD BAD - This is a possible race condition
	//  // another goroutine might alter m between the calls to Get() and Set()
	//  if value, ok := m.Get("a"); ok {
	//      m.Set("b", value)
	//  }
	//
	//  // Good
	//  // WithLock guarantees that all operations within the callback
	//  // are run within a single lock
	//  m.WithLock(func(lm LockedMap[string, int]) {
	//      if value, ok := lm.Get("a"); ok {
	//          lm.Set("b", value)
	//      }
	//  })
	//
	WithLock(func(LockedMap[K, V]))
}

// New creates a new map with an optional initial capacity
func New[K comparable, V any](capacity ...int) Map[K, V] {
	m := &gmap[K, V]{}
	if len(capacity) > 0 && capacity[0] > 0 {
		m.m = make(map[K]V, capacity[0])
	} else {
		m.m = map[K]V{}
	}
	return m
}

type gmap[K comparable, V any] struct {
	m map[K]V
	l sync.Mutex
}

func (m *gmap[K, V]) Set(key K, value V) {
	m.l.Lock()
	defer m.l.Unlock()
	m.set(key, value)
}

func (m *gmap[K, V]) set(key K, value V) {
	if m.m == nil {
		m.m = map[K]V{}
	}
	m.m[key] = value
}

func (m *gmap[K, V]) Get(key K) (value V, found bool) {
	m.l.Lock()
	defer m.l.Unlock()
	return m.get(key)
}

func (m *gmap[K, V]) get(key K) (value V, found bool) {
	if m.m == nil {
		return
	}
	v, ok := m.m[key]
	return v, ok
}

func (m *gmap[K, V]) Delete(keys ...K) {
	m.l.Lock()
	defer m.l.Unlock()
	m.delete(keys...)
}

func (m *gmap[K, V]) delete(keys ...K) {
	if m.m == nil {
		return
	}
	for _, key := range keys {
		delete(m.m, key)
	}
}

func (m *gmap[K, V]) Len() int {
	m.l.Lock()
	defer m.l.Unlock()
	return len(m.m)
}

func (m *gmap[K, V]) Reset() {
	m.l.Lock()
	m.reset()
	m.l.Unlock()
}
func (m *gmap[K, V]) reset() {
	m.m = map[K]V{}
}

func (m *gmap[K, V]) Keys() []K {
	m.l.Lock()
	defer m.l.Unlock()
	return m.keys()
}

func (m *gmap[K, V]) keys() []K {
	keys := make([]K, 0, len(m.m))
	for k := range m.m {
		keys = append(keys, k)
	}
	return keys
}

func (m *gmap[K, V]) WithLock(fn func(LockedMap[K, V])) {
	m.l.Lock()
	defer m.l.Unlock()
	lm := &lockedMap[K, V]{m}
	fn(lm)
}

type lockedMap[K comparable, V any] struct {
	m *gmap[K, V]
}

func (lm *lockedMap[K, V]) Set(key K, value V)              { lm.m.set(key, value) }
func (lm *lockedMap[K, V]) Get(key K) (value V, found bool) { return lm.m.get(key) }
func (lm *lockedMap[K, V]) Delete(keys ...K)                { lm.m.delete(keys...) }
func (lm *lockedMap[K, V]) Reset()                          { lm.m.reset() }
func (lm *lockedMap[K, V]) Len() int                        { return len(lm.m.m) }
func (lm *lockedMap[K, V]) Keys() []K                       { return lm.m.keys() }

// type RangeElement[K comparable, V any] struct {
// 	Key   K
// 	Value V
// }
// func (m *gmap[K, V]) Range() <-chan (RangeElement[K, V]) {
// 	c := make(chan (RangeElement[K, V]))
// 	go func() {
// 		m.l.Lock()
// 		defer m.l.Unlock()
// 		defer close(c)
//
// 		for k, v := range m.m {
// 			c <- RangeElement[K, V]{
// 				Key:   k,
// 				Value: v,
// 			}
// 		}
//
// 	}()
// 	return c
// }
