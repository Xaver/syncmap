package syncmap

import (
	"testing"
)

func TestMap1(t *testing.T) {
	m := New[string, int]()
	m.Set("a", 1)
	m.Set("b", 2)
	m.Set("c", 3)

	keys := m.Keys()
	if len(keys) != 3 {
		t.Fatal("should have 3 keys")
	}

	a, ok := m.Get("a")
	if !ok {
		t.Fatal("a should exist")
	}
	if a != 1 {
		t.Fatal("a should be 1")
	}

	m.Delete("a")

	if _, ok = m.Get("a"); ok {
		t.Fatal("a should be deleted")
	}
	m.Delete("b", "c")

	m.Reset()
	m.WithLock(func(mm LockedMap[string, int]) {
		mm.Set("d", 4)
		mm.Set("e", 5)
	})
	if len(m.Keys()) != 2 {
		t.Fatal("should have 2 keys")
	}
}
